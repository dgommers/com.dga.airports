package com.dga.airports;


import android.app.Application;

/**
 * Global Application object for managing the Database
 */
public class AirportsApplication extends Application {
    private LocalDatabase database;
    private static AirportsApplication sharedInstance;

    public LocalDatabase getDatabase() {
        if (database == null) {
            database = new LocalDatabase(getBaseContext());
        }
        return database;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedInstance = this;
    }

    public static AirportsApplication getSharedInstance() {
        return sharedInstance;
    }
}
