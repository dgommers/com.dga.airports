package com.dga.airports;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.SectionIndexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * IndexedAdapter for Alphabetic fastscrollling
 */
public class IndexedAdapter<T> extends ArrayAdapter<T> implements SectionIndexer
{
    private HashMap<String, Integer> alphaIndexer;
    private String[] sections;

    public IndexedAdapter(Context context, int resource, int textViewResourceId, List<T> objects)
    {
        super(context, resource, textViewResourceId, objects);

        alphaIndexer = new HashMap<String, Integer>();
        int size = objects.size();

        // Collect the letters
        for (int x = 0; x < size; x++) {
            String s = objects.get(x).toString();

            // Get the first letter of the store
            String ch = s.substring(0, 1);
            // Convert to uppercase otherwise lowercase a -z will be sorted
            // After upper A-Z
            ch = ch.toUpperCase();

            // Put only if the key does not exist
            if (!alphaIndexer.containsKey(ch)) alphaIndexer.put(ch, x);
        }

        Set<String> sectionLetters = alphaIndexer.keySet();

        // Create a list from the set to sort
        ArrayList<String> sectionList = new ArrayList<String>(
                sectionLetters);
        Collections.sort(sectionList);

        sections = new String[sectionList.size()];
        sectionList.toArray(sections);
    }

    public int getPositionForSection(int section)
    {
        return alphaIndexer.get(sections[section]);
    }

    public int getSectionForPosition(int position)
    {
        return 0;
    }

    public Object[] getSections()
    {
        return sections;
    }
}