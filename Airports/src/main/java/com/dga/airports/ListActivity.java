package com.dga.airports;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Main List Activity, displays a list of all airports
 */
public class ListActivity extends Activity {

    private ListView listView;
    private LocalDatabase database;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Progress Dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);

        // Initialize listView
        listView = (ListView) findViewById(R.id.listView);
        listView.setFastScrollAlwaysVisible(true);
        listView.setFastScrollEnabled(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Airport airport = (Airport) parent.getItemAtPosition(position);
                openDetailActivity(airport);
            }
        });

        // Open database async
        final Context context = this;
        new AsyncTask <Void, Void, IndexedAdapter<Airport>> () {

            @Override
            protected void onPreExecute ()
            {
                progressDialog.show();
            }

            @Override
            protected IndexedAdapter<Airport> doInBackground (Void... x)
            {
                AirportsApplication app = AirportsApplication.getSharedInstance();
                database = app.getDatabase();

                return new IndexedAdapter<Airport>(context, R.layout.activity_list_item, R.id.textView, database.getAirports());
            }

            @Override
            protected void onPostExecute(IndexedAdapter<Airport> adapter)
            {
                listView.setAdapter(adapter);
                progressDialog.hide();
            }

        }.execute();
    }

    /**
     * Handle detail clicks
     * @param airport
     */
    private void openDetailActivity (Airport airport) {
        Intent intent = new Intent(this, DetailActivity.class);
        int index = database
                .getAirports()
                .indexOf(airport);

        intent.putExtra(DetailActivity.INTENT_AIRPORT_INDEX, index);
        startActivity(intent);
    }

}