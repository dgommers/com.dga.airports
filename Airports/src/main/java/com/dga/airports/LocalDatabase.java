package com.dga.airports;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * LocalDatabase for handling the sqlite file
 */
public class LocalDatabase {

    // Constants
    private static final String DB_NAME = "airports.sqlite";
    private static final String TABLE_NAME = "airports";
    private static final String BASE_AIRPORT_ICAO = "EHAM";
    private String folder;
    private String path;

    private SQLiteDatabase db;
    private Context context;

    private ArrayList<Airport> airports;
    private Airport baseAirport;

    /**
     * Constructor
     * @param c parent context
     */
    public LocalDatabase(Context c) {
        context = c;
        folder = context.getString(R.string.data_path)+context.getPackageName()+"/databases";
        path = folder+"/"+DB_NAME;
        checkDB();
        db = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
    }


    /**
     * Check current DB status, if not exists, create!
     */
    public void checkDB() {
        try {
            File f = new File(folder);
            File obj = new File(path);

            if (!f.exists()) {
                f.mkdirs();
                f.createNewFile();
            }

            if (!obj.exists()) {
                CopyDB(path);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("X", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("X", e.getMessage());
        }
    }

    /**
     * Copy DB from assets
     * @param path
     * @throws IOException
     */
    public void CopyDB(String path) throws IOException {

        InputStream databaseInput = null;
        String outFileName = path;
        OutputStream databaseOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;

        // open database file from asset folder
        databaseInput = context.getAssets().open(DB_NAME);
        while ((length = databaseInput.read(buffer)) > 0) {
            databaseOutput.write(buffer, 0, length);
            databaseOutput.flush();
        }
        databaseInput.close();
        databaseOutput.flush();
        databaseOutput.close();
    }


    /**
     * Get ArrayList of Airprots
     * @return Airports
     */
    public ArrayList<Airport> getAirports ()
    {
        if (airports == null) {
            airports = getAirportsByQuery("SELECT * FROM "+TABLE_NAME+" ORDER BY name COLLATE NOCASE ASC");
        }
        return airports;
    }

    /**
     * Get airports by custom query
     * @param query
     * @return ArrayList
     */
    public ArrayList<Airport> getAirportsByQuery (String query)
    {
        ArrayList<Airport> result = new ArrayList<Airport>();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                Airport a = getAirportFromCursor(c);
                if (!a.getName().equals("")) {
                    result.add(a);
                }
                c.moveToNext();
            }
        }
        return result;
    }

    /**
     * Get the default base airport
     * @return
     */
    public Airport getBaseAirport ()
    {
        if (baseAirport == null) {
            ArrayList<Airport> result = getAirportsByQuery("SELECT * FROM "+TABLE_NAME+" WHERE icao = \""+BASE_AIRPORT_ICAO+"\" LIMIT 1");
            if (result.size() == 1) {
                baseAirport = result.get(0);
            }
        }
        return baseAirport;
    }

    /**
     * Convert cursor into Entities
     * @param c
     * @return
     */
    private Airport getAirportFromCursor (Cursor c)
    {
        Airport a = new Airport();
        List<String> columns = Arrays.asList(c.getColumnNames());
        a.setIcao(c.getString(columns.indexOf("icao")));
        a.setName(c.getString(columns.indexOf("name")));
        a.setLongitude(c.getDouble(columns.indexOf("longitude")));
        a.setLatitude(c.getDouble(columns.indexOf("latitude")));
        a.setElevation(c.getDouble(columns.indexOf("elevation")));
        a.setIsoCountry(c.getString(columns.indexOf("iso_country")));
        a.setMunicipality(c.getString(columns.indexOf("municipality")));
        return a;
    }

    /**
     * Utility to get TableNames from Sqlite file
     * @return
     */
    public ArrayList<String> getTableNames () {
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        ArrayList<String> list = new ArrayList<String>();
        if (c.moveToFirst()) {
            while (!c.isAfterLast()) {
                list.add(c.getString(0));
                c.moveToNext();
            }
        }
        return list;
    }
}