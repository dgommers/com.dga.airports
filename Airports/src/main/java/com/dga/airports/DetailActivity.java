package com.dga.airports;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Detail activity renders MapView
 */
public class DetailActivity extends Activity {

    public static final String INTENT_AIRPORT_INDEX = "airportIndex";
    private GoogleMap map;
    private Airport airport;
    private Airport baseAirport;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_detail);

        // Get the message from the intent
        Intent intent = getIntent();
        int index = intent.getIntExtra(INTENT_AIRPORT_INDEX, -1);

        // Receive Airport instance
        LocalDatabase db = AirportsApplication.getSharedInstance().getDatabase();
        this.airport = db.getAirports().get(index);
        this.baseAirport = db.getBaseAirport();

        // Set title
        this.setTitle(airport.toString());

        // Set the text view as the activity layout
        setContentView(R.layout.activity_detail);

        // Get map
        FragmentManager fragmentManager = getFragmentManager();
        MapFragment fragment = (MapFragment)fragmentManager.findFragmentById(R.id.map);
        this.map = fragment.getMap();
        this.setMap();
    }

    /**
     * Set the right Map Position
     */
    protected void setMapPosition() {

        // Set map position
        LatLngBounds.Builder builder = LatLngBounds.builder();
        builder.include(airport.getLatLng());
        builder.include(baseAirport.getLatLng());
        LatLngBounds bounds = builder.build();

        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
        map.moveCamera(CameraUpdateFactory.zoomTo(2));
    }

    /**
     * Do addtional Map configuration, add Polyline
     */
    private void setMap ()
    {
        this.map.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        map.addPolyline(new PolylineOptions()
                .add(airport.getLatLng())
                .add(baseAirport.getLatLng())
                .width(4)
                .color(Color.argb(255, 24, 149, 255))
                .geodesic(true)
        );

        this.addMarker(airport);
        this.addMarker(baseAirport);

        // Wait for layout to complete
        this.map.setOnCameraChangeListener(new OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition arg0) {
                setMapPosition();
                map.setOnCameraChangeListener(null);
            }
        });
    }

    /**
     * Add marker with airplane on airport entity
     * @param a
     */
    private void addMarker (Airport a)
    {
        map.addMarker(new MarkerOptions()
            .position(a.getLatLng())
            .title(a.getName())
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher))
            .anchor(0.5f, 0.5f)
            .flat(true)
        );
    }
}
